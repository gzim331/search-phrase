import os
import sys

if (len(sys.argv) == 1):
    print('usage:')
    print('arg 1: phrase (e.g: getValue() or \'getValue() === 7\')')
    print('OPTIONAL arg 2: initial directory (default: work/api)')
    exit()

phrase = sys.argv[1]
path = 'work/api'
if len(sys.argv) == 3:
    path = sys.argv[2]


def read_file(filename):
    f = open(filename, 'r')
    return f.read()


def get_files(path):
    for (pathdir, _, filenames) in os.walk(path):
        for filename in filenames:
            yield os.path.join(pathdir, filename)


list_files = get_files(path)

for filename in list_files:
    if phrase in read_file(filename):
        print(filename)
